import React, {Component} from 'react';
import './Home.css';
import { Controls, PlayState, Tween } from 'react-gsap';

export default class Home extends Component {
    render() {
        return (
        <div className="outer-container">
            <nav className="nav-bar">
                <Tween
                delay={0.3}
                duration={0.8}
                from={{opacity: '0', y: '20'}}
                stagger={0.15}
                >
                <div>
                    <ion-icon color="light" name="menu"></ion-icon>
                </div>
                <div className="language">
                    Eng
                </div>
                <div>
                    <ion-icon name="search"></ion-icon>
                </div>
                </Tween>
            </nav>
            <div style={{zIndex: '9999'}}>
            <Tween
                delay={1.1}
                duration={1}
                from={{opacity: '0', y: '20'}}
                stagger={0.5}
                >
                <div className="main-title">INTERSTELLAR</div>
                <div className="sub-title">In space no one can hear you scream</div>
                </Tween>
            </div>
            <div className="detail-container">
            <Tween
                delay={2.1}
                duration={0.8}
                from={{opacity: '0', y: '20'}}
                stagger={0.2}
                >
                <div className="story">
                    Professor Brand, a brilliant NASA physicist, is working on plans to save mankind by transporting Earth's population to a new home via a wormhole. But first, Brand must send former NASA pilot Cooper and a team of researchers through the wormhole and across the galaxy to find out which of three planets could be mankind's new home.
                </div>
                <div className="download-btn">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px"
                    viewBox="0 0 490.688 490.688">
                <path  d="M472.328,120.529L245.213,347.665L18.098,120.529c-4.237-4.093-10.99-3.975-15.083,0.262
                    c-3.992,4.134-3.992,10.687,0,14.82l234.667,234.667c4.165,4.164,10.917,4.164,15.083,0l234.667-234.667
                    c4.237-4.093,4.354-10.845,0.262-15.083c-4.093-4.237-10.845-4.354-15.083-0.262c-0.089,0.086-0.176,0.173-0.262,0.262
                    L472.328,120.529z"/>
                <path d="M245.213,373.415c-2.831,0.005-5.548-1.115-7.552-3.115L2.994,135.633c-4.093-4.237-3.975-10.99,0.262-15.083
                    c4.134-3.992,10.687-3.992,14.82,0l227.136,227.115l227.115-227.136c4.093-4.237,10.845-4.354,15.083-0.262
                    c4.237,4.093,4.354,10.845,0.262,15.083c-0.086,0.089-0.173,0.176-0.262,0.262L252.744,370.279
                    C250.748,372.281,248.039,373.408,245.213,373.415z"/>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                </svg>

                </div>
                <div className="play-container">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" >
                    <metadata> Svg Vector Icons : http://www.onlinewebfonts.com/icon </metadata>
                    <g><path d="M500,990C229.8,990,10,770.2,10,500C10,229.8,229.8,10,500,10c270.2,0,490,219.8,490,490C990,770.2,770.2,990,500,990z M500,56.7C255.5,56.7,56.7,255.5,56.7,500c0,244.5,198.9,443.3,443.3,443.3S943.3,744.5,943.3,500C943.3,255.5,744.5,56.7,500,56.7z"/><path d="M348.3,220l420,280l-420,280V220z"/></g>
                    </svg>

                </div>
                </Tween>
            </div>
        </div>
        )
    }
}